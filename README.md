# contract-manager-frontend

## Installation

Make sure you have installed NodeJS, polymer-cli, typescript.<br>
Make sure you have [contract-manager-server](https://gitlab.com/rammbulanz/contract-manager-server) set up and running

Then clone this repository and 
- `npm install` or `yarn install`
- `npm run build` or if you are coding, you can use `npm run watch`, to start webpack file watcher.
- Take a look into package.json at scripts -> serve and make sure the proxy configuration is compatible with your changes in the server's configuration.
- Start server either using `npm run serve` or your configured server.
- Page sould be available in your browser. It isn't? Well. shit.

## Usage

Dunno