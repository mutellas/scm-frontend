export interface IContract {
    id: string
    name: string
    category: string
    contractId: string
    accountId: string
    terminated: Boolean
    terminationPeriod: number | null
    terminationDate: number | null
    costInterval: string
    costs: number
    currency: string
}