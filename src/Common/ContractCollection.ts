import { IContract } from './IContract'

export interface ContractCollection {
    [id: string]: IContract
}