
/**
 * Read the current Profile from localStorage
 */
export function getProfileName() {
    return localStorage.getItem("profile");
}

/**
 * Read the current token from localStorage
 */
export function getToken(): string {
    return localStorage.getItem("token");
}

/**
 * 
 */
export async function authenticate(): Promise<boolean> {

    let user = prompt("Input your username");
    let password = prompt("Input your password");

    
    return false;
}

// Initiaize Profile Name
if (!getProfileName()) {
    let profileName = prompt("Please enter a new profile name");
    if (profileName) {
        localStorage.setItem("profile", profileName);
    }
}