import i18next from 'i18next'
import i18nextXhrBackend from 'i18next-xhr-backend'

export const locale = i18next
    .use(i18nextXhrBackend)
    .init({
        ns: ["common"],
        defaultNS: "common",
        fallbackLng: 'en',
        lng: "de",
        backend: {
            // path where resources get loaded from, or a function
            // returning a path:
            // function(lngs, namespaces) { return customPath; }
            // the returned path will interpolate lng, ns if provided like giving a static path
            loadPath: 'locales/{{lng}}/{{ns}}.json'
        }
    });

(window as any).locale = locale;

locale.on("failedLoading", (lng, ns, msg) => console.error(`Failed loading localization. LNG=${lng} NS=${ns} MSG=${msg}`))