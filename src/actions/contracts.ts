import { IContract } from '../Common/IContract'
import { ContractCollection } from '../Common/ContractCollection'
import { store } from '../store'
import { getProfileName, getToken } from '../Common/Session';

export const ADD_CONTRACT = "ADD_CONTRACT";
export const ADD_CONTRACTS = "ADD_CONTRACTS";
export const DELETE_CONTRACT = "DELETE_CONTRACT";
export const EDIT_CONTRACT = "EDIT_CONTRACT";

export const addContract = (contract: IContract) => store.dispatch({
   type: ADD_CONTRACT,
   contract
});

export const addContracts = (contracts: IContract[] | ContractCollection) => store.dispatch({
   type: ADD_CONTRACTS,
   contracts
});

export const deleteContract = (contract: IContract) => store.dispatch({
   type: DELETE_CONTRACT,
   contract
});

export const editContract = (contract: IContract) => store.dispatch({
   type: EDIT_CONTRACT,
   contract
});

export async function loadContracts() {
   
   let res = await fetch("/contracts/get/all", {
      method: "POST",
      body: JSON.stringify({
         profile: getProfileName(),
         token: getToken()
      }),
      headers: {
         'Accept': 'application/json, text/plain, */*',
         'Content-Type': 'application/json'
      }
   });

   if (res.status == 200) {
      let contracts = await res.json();
      addContracts(contracts)
   } else {
      console.error(new Error(res.statusText));
      alert(res.statusText);
   }

}
