import { createStore, combineReducers } from 'redux'
import contracts from './reducers/contracts';
import { LitElement } from '@polymer/lit-element';
import { connect } from 'pwa-helpers'

export const store = createStore(combineReducers({
   contracts
}));

/**
 * Extend LitElement by pwa-helpers/connect mixin
 */
export const ConnectedLitElement: { new(): LitElement } = connect(store)(LitElement)