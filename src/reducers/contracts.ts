import { IContract } from "../Common/IContract";
import { ADD_CONTRACT, DELETE_CONTRACT, EDIT_CONTRACT, ADD_CONTRACTS } from "../actions/contracts";
import { Action } from "redux";
import { ContractCollection } from "../Common/ContractCollection";

export interface ProcessContractsActions extends Action<string> {
	contract?: IContract
	contracts?: ContractCollection
}

export default function (state: ContractCollection = {}, action: ProcessContractsActions) {
	switch (action.type) {

		case ADD_CONTRACT:
			state[action.contract.id] = action.contract
			return state;

		case ADD_CONTRACTS:
			for (let id in action.contracts) {
				state[id] = action.contracts[id];
			}
			return state;

		case DELETE_CONTRACT:
			state[action.contract.id] = undefined; 
			return state;

		case EDIT_CONTRACT:
			return state;

		default:
			return state;

	}
}
