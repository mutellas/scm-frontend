import { property, customElement } from "@rammbulanz/lit-helpers";
import { LitElement, html } from "@polymer/lit-element/lit-element";
import { IContract } from "../Common/IContract";
import { Categories, Currencies } from "../Common/Lists";
import uuid from '../Common/uuid'
import { getProfileName, getToken } from "../Common/Session";
import { renderContractDetails } from "./contract-details";
import '@polymer/paper-dialog/paper-dialog'
import '@vaadin/vaadin-button/vaadin-button'

interface CreateContractMaskProps {
    contract: IContract
}

function createNewContract(): IContract {
    return {
        id: uuid(),
        name: "",
        category: Categories[0],
        accountId: "",
        contractId: "",
        costInterval: "monthly",
        costs: 0,
        currency: Currencies[0],
        terminated: false,
        terminationDate: null,
        terminationPeriod: null
    }
}

@customElement("contract-create")
class CreateContractMask extends LitElement implements CreateContractMaskProps {

    // Polyfill
    shadowRoot: ShadowRoot

    // Properties
    @property("contract")
    contract: IContract = createNewContract()

    public get contractMask(): any {
        return this.shadowRoot.querySelector("contract-details");
    }

    protected _render({ contract }: CreateContractMaskProps) {
        return html`

            <style>
                :host {
                    padding: 20px;
                }
            </style>
            <style>
                .form-items {
                    display: table;
                }

                .form-items>div {
                    display: table-row;
                }

                .form-items>div>* {
                    display: table-cell;
                    text-align: left;
                    padding: 7px 7px;
                }

                input,
                select {
                    width: 100%;
                }
            </style>


            <h2> Create new Contract Entry </h2>
            ${renderContractDetails(contract, () => this.requestRender())}

            <div class="button">
                <vaadin-button dialog-confirm on-click="${(e: any) => this._create(e)}">Create</vaadin-button>
                <vaadin-button dialog-dismiss>Dismiss</vaadin-button>
                <vaadin-button on-click="${() => this._reset()}">Clear</vaadin-button>
            </div>

      `;
    }

    private async _create(e: MouseEvent) {

        try {

            if (e.shiftKey) {
                alert(JSON.stringify(this.contract, undefined, 2));
                return;
            }

            const contract = this.contract;

            // Check parameters

            if (!contract.name || contract.name.length == 0) {
                throw "Parameter name missing"
            }

            // Start Upload

            await this._uploadAndSubmit();

        } catch (err) {
            console.error(err);
            alert(err.message || err);
        }

    }

    private _reset() {
        this.contract = createNewContract();
        this.requestRender();
    }

    private async _uploadAndSubmit() {

        let res = await fetch("/contracts/create/new", {
            method: "POST",
            body: JSON.stringify({
                profile: getProfileName(),
                token: getToken(),
                contract: this.contract
            }),
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
            }
        });

        if (res.status == 200) {

            let result = await res.json() as { success: boolean, message?: string };

            if (result.success) {
                alert("Sucessfully added");
            } else {
                alert("Failed adding contract.\n" + result.message);
            }

        } else {
            let message = res.status + " - " + res.statusText;
            console.error(message);
            alert(message);
        }

    }

}
