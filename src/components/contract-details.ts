import { html } from "@polymer/lit-element/lit-element";
import { IContract } from "../Common/IContract";
import { Categories } from "../Common/Lists";
import { locale } from '../localization'

import '@polymer/paper-dropdown-menu/paper-dropdown-menu'
import '@polymer/paper-item/paper-item'
import '@polymer/paper-listbox/paper-listbox'
import '@vaadin/vaadin-text-field/vaadin-text-field';
import '@vaadin/vaadin-item/vaadin-item'
import './elements/locale-text'

type DCC = (key: string, value: any) => void;

export const renderContractDetails = (contract: IContract, detailChangedCallback: DCC) => html`

	<style>
		:host {
			padding: 20px;
		}

		.root {
			display:flex;
			flex-direction: column;
		}
	</style>

	<div>
		<div>
			<vaadin-text-field label="Name" value="${contract.name}" on-change="${(e) => detailChangedCallback("name", e.target.value)}"></vaadin-text-field>
			<paper-dropdown-menu label="Category">
				<paper-listbox slot="dropdown-content" selected="${contract.category}">
					${Categories.map(c => html`<paper-item value="${c}">${c}</paper-item>`)}
				</select>
			</paper-dropdown-menu>
		</div>
		<div>
			<vaadin-text-field label="Account ID" value="${contract.accountId}" on-change="${(e) => detailChangedCallback("accountId", e.target.value)}"></vaadin-text-field>
			<vaadin-text-field label="Contract ID" value="${contract.contractId}" on-change="${(e) => detailChangedCallback("contractId", e.target.value)}"></vaadin-text-field>
		</div>
		<div>
			<paper-dropdown-menu label="Costs interval">
				<paper-listbox slot="dropdown-content" >
					${["weekly", "monthly", "yearly"].map(c => html`
						<paper-item value="${c}">
							<locale-text localeKey="${locale.t(`common:interval.${c}`)}"></locale-text>
						</paper-item>
					`)}
				</paper-listbox>
			</paper-dropdown-menu>
			<vaadin-text-field label="Costs" value="${contract.costs}" on-change="${(e) => detailChangedCallback("costs", Number(e.target.value))}">
				<div slot="prefix">${contract.currency}</div>
			</vaadin-text-field>
		</div>
	</div>

`;