import { LitElement, html } from "@polymer/lit-element/lit-element";
import { IContract } from "../Common/IContract";
import { customElement, property } from "@rammbulanz/lit-helpers";
import { CardStyles, CardStylesHover } from '@rammbulanz/lit-card/lit-card-styles'

import './contract-edit'
import '@polymer/paper-dialog/paper-dialog'
import '@polymer/iron-icons/iron-icons'
import '@polymer/paper-icon-button/paper-icon-button'

interface ContractProps {
  contract: IContract
}

@customElement("x-contract")
class Contract extends LitElement implements ContractProps {

  @property(Object)
  contract: IContract

  public get editDialog() {
    return this.shadowRoot.querySelector("paper-dialog#edit") as HTMLElement & {
      open: () => void
    };
  }

  protected _render({ contract }: ContractProps) {

    if (!contract) {
      return html`<b>Contract is not defined</b>`;
    }

    // Check props
    if (!contract.currency) {
      contract.currency = "€";
    }

    return html`
         ${CardStyles} ${CardStylesHover}

        <style>
            :host {
                display: table-row;
                padding-top: 10px;
                padding-bottom: 10px;
            }

            :host>div {
                display: table-cell;
                padding: 3px 0px;
                border-top: 0.5px solid #000;
                border-top: 0.5px solid #000;
            }
        </style>

        <div>${contract.name}</div>
        <div>Category: ${contract.category}</div>
        <div>Account: ${contract.accountId}</div>
        <div>Costs interval: ${contract.costInterval}</div>
        <div>${contract.costs} ${contract.currency}</div>

        <div>
          <paper-icon-button icon="create" on-click="${() => this.editDialog.open()}"></paper-icon-button>
        </div>

        <paper-dialog id="edit">
          <div>
            <contract-edit contract="${contract}"></contract-edit>
          </div>
        </paper-dialog>
    `
  }

}
