import { LitElement, html } from "@polymer/lit-element";
import { locale } from "../../localization";
import { Constructor } from "../../types/Contstructor";

let _localeIdCouter = 0;
let _registeredTexts = new Map<number, LitElement>();

const refreshAll = () => {
    for (let element of _registeredTexts.values()) {
        element.requestRender();
    }
}

locale.on("languageChanged", refreshAll);
locale.on("loaded", refreshAll);


export const LocaleMixIn = (superClass: Constructor<LitElement>) => class extends superClass {

    private readonly _localeId: number = _localeIdCouter++;

    connectedCallback() {
        super.connectedCallback();
        _registeredTexts.set(this._localeId, this);
    }

    disconnectedCallback() {
        _registeredTexts.delete(this._localeId);
        super.disconnectedCallback();
    }

}

interface LocaleTextProps {
    localeKey: string
    localeValue: string
}

/**
 * A element, that will render each time, localization changes
 */
export class LocaleText extends LocaleMixIn(LitElement) implements LocaleTextProps {

    static get properties() {
        return {
            localeKey: String,
            localeValue: String
        }
    }

    localeKey: string
    localeValue: string

    _render({ localeValue }: LocaleTextProps) {
        return html`<span>${localeValue}</span>`;
    }

    _propertiesChanged(props: LocaleTextProps, changed: LocaleTextProps, prev: LocaleTextProps) {

        if (changed && changed.localeKey) {
            this.localeValue = locale.t(changed.localeKey);
        }

        return super._propertiesChanged(props, changed, prev);
    }

}

customElements.define("locale-text", LocaleText);