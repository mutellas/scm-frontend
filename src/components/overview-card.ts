import { LitElement, html } from '@polymer/lit-element/lit-element.js'
import { CardStyles } from '@rammbulanz/lit-card/lit-card-styles.js'

class OverviewCard extends LitElement {

    static get properties() {
        return {
            value: String,
            label: String
        }
    }

    _render({ label, value }) {
        return html`
            ${CardStyles}
            <style>
                :host {
                    display: block;
                }
            
                .content {
                    padding: 40px;
                }
            
                .number {
                    font-size: 3em;
                }
                            
                .label {
                    font-size: 1.8em;
                }
            </style>
            <div class="content">
                <div class="number">
                    <span>${value}</span>
                </div>
                <span class="label">${label}</label>
            </div>
        `
    }

}

customElements.define("overview-card", OverviewCard);