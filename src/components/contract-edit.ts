import { LitElement, html } from "@polymer/lit-element/lit-element";
import { IContract } from "../Common/IContract";
import * as lodash from 'lodash-es'
import { getProfileName, getToken } from "../Common/Session";
import { customElement, property } from "@rammbulanz/lit-helpers";
import { renderContractDetails } from "./contract-details";
import '@polymer/paper-dialog/paper-dialog'
import '@vaadin/vaadin-button/vaadin-button'

interface CotnractEditProps {
	contract: IContract
	editContract: IContract
}

@customElement("contract-edit")
class ContractEdit extends LitElement implements CotnractEditProps {

	@property(Object)
	contract: IContract
	@property(Object)
	editContract: IContract

	
	_propertiesChanged(props: CotnractEditProps, changed: CotnractEditProps, prev: CotnractEditProps) {

		if (changed) {
			if (changed.contract && !this.editContract) {
				this.editContract = lodash.cloneDeep(changed.contract);
			}
		}

		super._propertiesChanged(props, changed, prev);
	}

	_contractDetailChanged(key: string, value: any) {
		this.editContract[key] = value;
	}

	_render({ editContract }: CotnractEditProps) {
		return html`

			<style>
				:host {
						padding: 20px;
				}
			</style>


			<h2> Edit contract </h2>
			<div>
					${renderContractDetails(editContract, (key, value) => this._contractDetailChanged(key, value))}
			</div>

			<div class="button">
					<vaadin-button dialog-confirm on-click="${(e: any) => this.submitChanges(e)}">Save</vaadin-button>
					<vaadin-button dialog-dismiss on-click="${() => this._reset()}">Close</vaadin-button>
					<vaadin-button on-click="${() => this._reset()}">Reset</vaadin-button>
			</div>

		`;
	}

	private _reset() {
		this.editContract = lodash.cloneDeep(this.contract);
	}

	async submitChanges(e: MouseEvent) {

		try {

			const contract = this.contract = this.editContract;

			if (e.shiftKey) {
				alert(JSON.stringify(contract, undefined, 2));
				return;
			}

			// Check parameters

			if (!contract.name || contract.name.length == 0) {
				throw "Parameter name missing"
			}

			// Start Upload

			await this._uploadAndSubmit();

		} catch (err) {
			console.error(err);
			alert(err.message || err);
		}

	}

	private async _uploadAndSubmit() {

		const contract = this.contract;

		let res = await fetch("/contracts/edit", {
			method: "POST",
			body: JSON.stringify({
				profile: getProfileName(),
				token: getToken(),
				contract: contract
			}),
			headers: {
				'Accept': 'application/json, text/plain, */*',
				'Content-Type': 'application/json'
			}
		});

		if (res.status == 200) {

			let result = await res.json() as { success: boolean, message?: string };

			if (result.success) {
				alert("Sucessfully saved");
			} else {
				alert("Failed saving contract.\n" + result.message);
			}

		} else {
			let message = res.status + " - " + res.statusText;
			console.error(message);
			alert(message);
		}

	}

}


