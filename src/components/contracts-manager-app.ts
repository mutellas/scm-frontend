import { LitElement, html } from '@polymer/lit-element/lit-element'
import { connect } from 'pwa-helpers/connect-mixin'

import '@polymer/paper-dialog/paper-dialog.js'
import '@vaadin/vaadin-button/vaadin-button.js'
import '@polymer/iron-icons/iron-icons.js'
import '@polymer/paper-icon-button/paper-icon-button.js'
import '@polymer/app-layout/app-layout.js'

import './contract-row'
import './contract-create';
import './overview-card'
import './elements/locale-text'

import { IContract } from '../Common/IContract';

import { ConnectedLitElement } from '../store'
import { loadContracts } from '../actions/contracts';
import { ContractCollection } from '../Common/ContractCollection';
import { customElement } from '@rammbulanz/lit-helpers';
import { render } from 'lit-html';
import { locale } from '../localization';
import { LocaleMixIn } from './elements/locale-text';

@customElement("contract-manager-app")
class ContractManagerApp extends LocaleMixIn(ConnectedLitElement) {

    public static get properties() {
        return {
            contracts: Object,
            activeContracts: Array,
            costsPerMonth: Number
        }
    }

    // Local

    public contractsArray = new Array<IContract>();
    public activeContracts = new Array<IContract>();
    public costsPerMonth = 0;

    constructor() {
        super();
        loadContracts();
    }

    private contracts: ContractCollection

    public get drawer() {
        return this.shadowRoot.querySelector("#drawer") as HTMLElement & {
            toggle: () => void
        };
    }
    public get createContractElement() {
        return document.body.querySelector("#create-contract");
    }

    protected _render() {

        return html`

            <style>
                .overview {
                    display: flex;
                    width: 100%;
                    overflow-x: auto;
                }

                .overview>* {
                    flex-grow: 1;
                }

                .your-contracts {
                    display: flex;
                    flex-direction: column;
                }
                .active-contracts {
                    display: table;
                }
                .contract:hover {
                    background: #ddd;
                }
            </style>

            <app-header>
                <app-toolbar>
                    <paper-icon-button icon="menu" onclick="${() => this.drawer.toggle()}"></paper-icon-button>
                    <div main-title>${locale.t("common:title")}</div>
                </app-toolbar>
                <div class="overview">
                    <overview-card value="${this.costsPerMonth + "€"}" label="${locale.t("common:messages.costs-per-month")}"></overview-card>
                    <overview-card value="${this.activeContracts.length}" label="${locale.t("common:messages.active-contracts")}"></overview-card>
                </div>
            </app-header>
            <app-drawer id="drawer"></app-drawer>
            <div class="your-contracts">

                <div class="active-contracts">
                    ${(this.contractsArray.length <= 0)
                        ? html`<b>No active contracts</b>`
                        : this.activeContracts.map(contract => this.renderContract(contract))}
                </div>

                <vaadin-button class="add-entry" on-click="${() => this.openNewContract()}"> Add </vaadin-button>

            </div>
        `;
    }


    public openNewContract() {

        let contractElement: Element = this.createContractElement;

        if (!contractElement) {

            const root = document.body.querySelector("#dialog-container");

            render(html`
                <paper-dialog id="create-contract" raise modal autofocus>
                    <div>
                        <contract-create></contract-create>
                    </div>
                </paper-dialog>
            `, root);

            contractElement = this.createContractElement;

        }

        (contractElement as any).open();

    }

    public renderContract(contract: IContract) {
        return html`
            <x-contract class="contract" contract="${contract}"></x-contract>
        `;
    }


    public calculateCostsPerMonth(contracts: Array<IContract>) {

        if (!contracts || Object.keys(contracts).length == 0)
            return 0;

        const _contractMontly = (contract: IContract) => {
            switch (contract.costInterval.toLowerCase()) {
                case "yearly":
                    return contract.costs / 12;
                default:
                    return contract.costs;
            }
        };

        const sum = contracts
            .map(_contractMontly)
            .reduce((accumulator, currentValue) => accumulator + currentValue);

        return Math.round(sum * 100) / 100;
    }

    // This is used by pwa-helpers to tell, that contracts store has been changed
    _stateChanged(state: { contracts: ContractCollection }) {

        if (state) {

            // Update properties
            this.contracts = state.contracts;
            this.contractsArray = Object.keys(this.contracts).map(id => this.contracts[id]);
            this.activeContracts = this.contractsArray.filter(c => !c.terminated);
            this.costsPerMonth = this.calculateCostsPerMonth(this.activeContracts);

            this.requestRender();

        }

    }

}